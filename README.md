# Urban Transit Network Design Problem #

Optimization algorithms for solving the 5 stages of the urban transit network design problem (UTNDP) identified by Ceder & Wilson 1986:

1. Network Design: the urban transit routing problem (UTRP)
2. Setting Frequencies: the urban transit scheduling problem (UTSP)
3. Timetable Development
4. Bus Scheduling
5. Driver Scheduling