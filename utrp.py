#!/usr/bin/env python
#
# Solve the multi-objective Urban Transit Routing Problem (UTRP),
# popularly called "the Bus Routing Problem", using the
# Non-dominated Sorting Genetic Algorithm II (NSGAII)
#
# Objectives:
# 1: Passenger costs = average travel time of all passengers
# 2: Operator costs = total length of bus routes
#
# References:
# [1] Cooper, Ian, John, Matthew P., Lewis, Rhydian, Mumford, Christine Lesley and Olden, Andrew.
# Optimising large scale public transport network design problems using mixed-mode parallel multi-objective
# evolutionary algorithms. Evolutionary Computation (CEC), 2014 IEEE Congress on. IEEE, 2014.
# http://dx.doi.org/10.1109/CEC.2014.6900362
#
# [2] John, Matthew P., Christine L. Mumford, and Rhyd Lewis. An improved multi-objective algorithm for the
# urban transit routing problem. Evolutionary Computation in Combinatorial Optimisation. Springer Berlin
# Heidelberg, 2014. 49-60.
# http://dx.doi.org/10.1007/978-3-662-44320-0_5
#
# [3] Mumford, Christine L, New heuristic and evolutionary operators for the multi-objective urban transit
# routing problem, Evolutionary Computation (CEC), 2013 IEEE Congress on , vol. 1, pp.939,946, 20-23 June
# 2013.
# http://dx.doi.org/10.1109/CEC.2013.6557668
#
# [4] Fan, Lang, Mumford, Christine L, Evans, Dafydd. A simple multi-objective optimization algorithm for the
# urban transit routing problem. IEEE Congress on Evolutionary Computation, pp 1-7, 2009.
# http://dx.doi.org/10.1109/CEC.2009.4982923
#
# [5] Fan, Lang, and Mumford, Christine.L. A metheuristic approach to the urban transit routing problem.
# Journal of Heuristics, Vol 16 (3) 2010 pp 353-372.
# http://dx.doi.org/10.1007/s10732-008-9089-8
# [6] Fan, Lang. 2009. Metaheuristic Methods for the Urban Transit Routing Problem. PhD Thesis, Cardiff
# University.
# https://users.cs.cf.ac.uk/C.L.Mumford/Research%20Topics/UTRP/papers/Lang_thesis.pdf
#
# [7] John, M. P. 2016. Metaheuristics for designing efficient routes & schedules for urban transportation
# networks. PhD Thesis, Cardiff University.
# http://orca.cf.ac.uk/99851
#
# [8] Mandl, Christoph E. Applied network optimization. Academic Press, 1979.
#
# [9] Shih, M. C. and Mahmassani, H. S. A design methodology for bus transit networks
# with coordinated operations. Technical Report SWUTC/94/60016-1, 1994.
#
# [10] Brownlee, Jason. Clever algorithms: nature-inspired programming recipes. Jason Brownlee, 2011.
#
# Author: Kwabena N. Amponsah

from __future__ import absolute_import, print_function, division
from random import random, randint, choice as randchoice, shuffle as randshuffle
from copy import copy, deepcopy
from matplotlib import pyplot as plt
from matplotlib import patheffects
import numpy as np
import cProfile
import os

# ========== CONSTANTS
INF = float('inf')
NAN = float('nan')

DATASET = ""
COORDS_FILE = ""
DEMAND_FILE = ""
TTIMES_FILE = ""

N = 0         # Number of nodes
COORDS = []   # Node coordinates

DEMANDS = []
TOTAL_DEMAND = 0
NORM_DEMANDS = []  # Normalized demands

TTIMES = []
NORM_TTIMES = []  # Normalized travel times

ADJ = []  # Node adjacencies

ALL_RDIST = []  # Overall shortest travel times
ALL_RNEXT = []  # Overall shortest paths

DEMAND_PAIRS = []  # All node pairs, sorted by demand descending

TRANSFER_PENALTY = 5  # Five-minute transfer penalty

R_NUM = 0      # Number of routes per routeset
R_MIN = 0      # Minimum number of nodes per route
R_MAX = 0      # Maximum number of nodes per route
P_CROSS = 0    # Crossover probability
P_MUTATE = 0   # Mutation probability
POP_SIZE = 0   # Population size
MAX_GENS = 0   # Maximum number of generations


# ========== INITIALIZATION
def load_data(dataset="Mandl"):
    # TODO: FILE ERROR & DATA CHECKING
    global DATASET, COORDS_FILE, DEMAND_FILE, TTIMES_FILE
    global N, TOTAL_DEMAND, COORDS, DEMANDS, TTIMES, ADJ
    global NORM_TTIMES, NORM_DEMANDS
    global ALL_RDIST, ALL_RNEXT, DEMAND_PAIRS

    # Data
    path = os.path.dirname(os.path.realpath(__file__))
    DATASET = dataset
    COORDS_FILE = "{0}/instances/{1}/{1}Coords.txt".format(path, DATASET)
    DEMAND_FILE = "{0}/instances/{1}/{1}Demand.txt".format(path, DATASET)
    TTIMES_FILE = "{0}/instances/{1}/{1}TravelTimes.txt".format(path, DATASET)

    # Coordinates
    with open(COORDS_FILE) as inputfile:
        N = int(inputfile.readline())

        for line in inputfile:
            coord = line.strip().split()
            if coord:
                COORDS.append(list(map(float, coord)))

    # Demands
    with open(DEMAND_FILE) as inputfile:
        for line in inputfile:
            demand = line.strip().split()
            if demand:
                DEMANDS.append(list(map(float, demand)))

    TOTAL_DEMAND = sum([sum(row) for row in DEMANDS]) / 2

    # Travel times & node adjacencies
    with open(TTIMES_FILE) as inputfile:
        for line in inputfile:
            ttime = line.strip().split()
            if ttime:
                tt = list(map(float, ttime))
                TTIMES.append(tt)
                ADJ.append(set([i for i, t in enumerate(tt) if 0.0 < t < INF]))

    # Normalize times and demands -> for john_routeset()
    max_time = max([cell for row in TTIMES for cell in row if cell < INF])
    max_demand = max([cell for row in DEMANDS for cell in row])

    NORM_TTIMES = [[0.0]*N for i in range(N)]
    NORM_DEMANDS = [[0.0]*N for i in range(N)]

    for row in range(N):
        for col in range(N):
            t = TTIMES[row][col]
            d = DEMANDS[row][col]

            if (t == INF) or (row == col):
                NORM_TTIMES[row][col] = INF
            else:
                NORM_TTIMES[row][col] = t/max_time

                NORM_DEMANDS[row][col] = d/max_demand

    # Sort all node pairs  by demand descending -> for generate_route()
    for i in range(N):
        for j in range(i+1, N):
            DEMAND_PAIRS.append((i, j, DEMANDS[i][j]))
    DEMAND_PAIRS.sort(key=lambda x: x[-1], reverse=True)

    # Overall distances and shortest paths
    ALL_RDIST, ALL_RNEXT = floyd_warshall(ADJ)


def set_params(r_num, r_min, r_max, p_cross, pop_size, max_gens):
    # TODO: PARAMETER VALIDATION
    global R_NUM, R_MIN, R_MAX, P_CROSS, P_MUTATE, POP_SIZE, MAX_GENS
    R_NUM = r_num
    R_MIN = r_min
    R_MAX = r_max
    P_CROSS = p_cross
    P_MUTATE = 1.0/r_num
    POP_SIZE = pop_size
    MAX_GENS = max_gens


# ========== GRAPH ALGORITHMS
def route_adj(routeset):
    # Get node adjacencies from routeset
    adj = [set() for i in range(N)]
    for r in routeset:
        for i in range(len(r)-1):
            adj[r[i]].add(r[i+1])
            adj[r[i+1]].add(r[i])
    return adj


def is_connected(adj):
    # Check if graph is connected
    n = len(adj)

    openset = set(range(n))
    visited = {0}

    while len(visited) < n:
        for i in openset:
            if i in visited:
                u = i
                openset.remove(i)
                break
        else:
            # No connection from openset to visited set
            return False

        for v in adj[u]:
            visited.add(v)

    return True


def floyd_warshall(adj):
    # Floyd & Warshall's all-to-all shortest paths algorithm
    n = len(adj)
    rdist = []  # Routing distance matrix
    rnext = []  # Routing "next node" matrix

    for u in range(n):
        rdist.append([INF]*n)
        rdist[u][u] = 0
        rnext.append([None]*n)

        for v in adj[u]:
            rdist[u][v] = TTIMES[u][v]
            rnext[u][v] = v

    for k in range(n):
        for i in range(n):
            for j in range(n):
                if rdist[i][j] > (rdist[i][k] + rdist[k][j]):
                    rdist[i][j] = rdist[i][k] + rdist[k][j]
                    rnext[i][j] = rnext[i][k]

    return rdist, rnext


def floyd_warshall_path(rnext, source, target):
    # Extract path from the Floyd-Warshall algorithm
    if rnext[source][target] is None:
        return []

    u = source
    v = target

    path = [u]
    while u != v:
        u = rnext[u][v]
        path.append(u)

    return path


def dijkstra(adj, source):
    # Dijkstra's one-to-all shortest paths algorithm
    n = len(adj)
    openset = list(range(n))
    rdist = [INF]*n  # Routing distances from source
    rprev = [None]*n  # Routing "previous node" list

    rdist[source] = 0
    while openset:
        min_open = INF
        for i in openset:
            if rdist[i] < min_open:
                u = i
                min_open = rdist[i]

        if min_open == INF:  # Disconnected graph
            break

        openset.remove(u)

        for v in adj[u]:
            alt = rdist[u] + TTIMES[u][v]

            if alt < rdist[v]:
                rdist[v] = alt  # Shorter path found
                rprev[v] = u

    return rdist, rprev


def dijkstra_path(rprev, target):
    # Extract path from Dijkstra's algorithm
    path = []
    u = target

    while rprev[u] is not None:
        path.append(u)
        u = rprev[u]
    path.append(u)
    path.reverse()

    return path


def k_shortest_paths(adj, source, target, k):
    # Yen's k-shortest paths algorithm
    graph = deepcopy(adj)
    n = len(graph)

    dist, prev = dijkstra(graph, source)
    paths = [(dijkstra_path(prev, target), dist[target])]

    if not paths[0][0]:
        return []

    heap = []

    for pn in range(k-1):
        curr_path = paths[pn][0]
        for i in range(len(curr_path)-1):
            spur_node = curr_path[i]
            root_path = curr_path[0:i+1]

            removed_edges = set()
            for path, cost in paths:
                if root_path == path[0:i+1]:
                    u, v = path[i], path[i+1]
                    try:
                        graph[u].remove(v)
                        graph[v].remove(u)
                    except KeyError:
                        pass

                    removed_edges.add((u, v))
                    removed_edges.add((v, u))

            for node in root_path:
                if node == spur_node:
                    continue
                for u in range(n):
                    try:
                        graph[u].remove(node)
                        graph[node].remove(u)
                    except KeyError:
                        pass

                    removed_edges.add((u, node))
                    removed_edges.add((node, u))

            dist, prev = dijkstra(graph, spur_node)
            spur_dist = dist[target]

            if spur_dist < INF:
                spur_path = dijkstra_path(prev, target)
                total_path = root_path[:-1] + spur_path

                # TODO: total_dist FROM DIJKSTRA?
                total_dist = 0
                for j in range(len(total_path) - 1):
                    u = total_path[j]
                    v = total_path[j+1]
                    total_dist += TTIMES[u][v]

                k_path = (total_path, total_dist)
                if k_path not in heap:
                    heap.append(k_path)

            for u, v in removed_edges:
                graph[u].add(v)

        if heap:
            heap.sort(key=lambda h: h[-1], reverse=True)
            paths.append(heap.pop())
        else:
            break

    return paths


# ========== FITNESS EVALUATION
def min_transfers(routeset, path):
    # Min num of transfers to travel on path. From Fan 2009 thesis
    # TODO: TRY TRANSIT NETWORK FROM JOHN THESIS
    for r in routeset:
        if (path[0] in r) and (path[-1] in r):
            return 0

    transfers = 0
    r_num = len(routeset)
    markers = [True]*r_num

    for i in range(len(path)):
        # Try to follow an old trail
        for rn in range(r_num):
            if markers[rn]:
                markers[rn] = path[i] in routeset[rn]

        # If trail runs cold, switch
        if not any(markers):
            transfers += 1
            for rn in range(r_num):
                markers[rn] = path[i-1] in routeset[rn] and \
                              path[i] in routeset[rn]

    return transfers


def passenger_cost(routeset, rdist, rnext):
    cost = 0

    for u in range(N-1):
        for v in range(u+1, N):
            path = floyd_warshall_path(rnext, u, v)
            penalty = TRANSFER_PENALTY * min_transfers(routeset, path)
            cost += DEMANDS[u][v] * (rdist[u][v] + penalty)

    # Dividing by total demand is unnecessary (Fan 2009's formulation)
    return cost / TOTAL_DEMAND


def operator_cost(routeset, rdist):
    cost = 0
    for r in routeset:
        for i in range(len(r)-1):
            u, v = r[i], r[i+1]
            cost += rdist[u][v]
    return cost


def calculate_objectives(pop):
    for p in pop:
        rdist, rnext = floyd_warshall(route_adj(p['routeset']))
        p['objectives'] = [passenger_cost(p['routeset'], rdist, rnext),
                           operator_cost(p['routeset'], rdist)]


def calculate_quality(routeset):
    # Calculate quality metrics of routeset
    # Metrics are defined in Fan Mumford 2010
    rdist, rnext = floyd_warshall(route_adj(routeset))

    att = passenger_cost(routeset, rdist, rnext)  # ATT = Average travel time
    rl = operator_cost(routeset, rdist)           # RL = Total route length

    d0 = 0.0   # % of demand satisfied with no transfers
    d1 = 0.0   # % of demand satisfied with one transfer
    d2 = 0.0   # % of demand satisfied with two transfers
    dun = 0.0  # % of demand unsatisfied with two or fewer transfers

    shortest = INF  # Length of shortest route
    longest = 0     # Length of longest route

    # Calculate demand satisfaction percentages: d0...dun
    for u in range(N-1):
        for v in range(u+1, N):
            path = floyd_warshall_path(rnext, u, v)
            transfers = min_transfers(routeset, path)

            if transfers == 0:
                d0 += DEMANDS[u][v]

            elif transfers == 1:
                d1 += DEMANDS[u][v]

            elif transfers == 2:
                d2 += DEMANDS[u][v]

            else:
                dun += DEMANDS[u][v]

    d0 = d0 * 100.0 / TOTAL_DEMAND
    d1 = d1 * 100.0 / TOTAL_DEMAND
    d2 = d2 * 100.0 / TOTAL_DEMAND
    dun = dun * 100.0 / TOTAL_DEMAND

    # Find shortest and longest routes
    for r in routeset:
        if len(r) < shortest:
            shortest = len(r)

        if len(r) > longest:
            longest = len(r)

    return att, rl, d0, d1, d2, dun, shortest, longest


# ========== SORTING & SELECTION
def dominates(p1, p2):
    # Dominance; if p1==p2, p1 wins
    a = p1['objectives'][0] <= p2['objectives'][0]
    b = p1['objectives'][1] <= p2['objectives'][1]

    if a and b:
        return 1  # p1 dominates p2

    if a or b:
        return 0  # neither dominates the other

    return -1  # p2 dominates p1


def fast_nondominated_sort(pop):
    # Adapted from Brownlee's clever algorithms
    # Rank solutions, then store by rank in fronts
    pop_size = len(pop)
    fronts = [[]]  # Stores solutions by rank
    dom_set = [[] for i in range(pop_size)]  # Solutions p dominates
    dom_count = [0]*pop_size  # Count of p's dominating solutions

    # Determine solution dominance
    for i in range(pop_size):
        p1 = pop[i]
        for j in range(i+1, pop_size):
            p2 = pop[j]
            dom = dominates(p1, p2)
            if dom == 1:
                dom_set[i].append(j)
                dom_count[j] += 1
            elif dom == -1:
                dom_set[j].append(i)
                dom_count[i] += 1

        if 0 == dom_count[i]:  # p1 is non-dominated
            p1['rank'] = 0  # Highest rank is 0
            fronts[0].append(i)

    # Populate fronts by rank
    fn = 0  # Front number
    while True:
        next_front = []
        for i in fronts[fn]:
            for j in dom_set[i]:
                p2 = pop[j]
                dom_count[j] -= 1

                if 0 == dom_count[j]:  # No other solution dominates p2
                    p2['rank'] = fn + 1  # p2 belongs in next front
                    next_front.append(j)

        fn += 1

        if next_front:
            fronts.append(next_front)
        else:
            # No more solutions to sort
            break

    return fronts


def calculate_crowding_distance(pop, front):
    # Adapted from Brownlee's clever algorithms
    # Assign crowding distances to solutions in single front
    for i in front:
        pop[i]['cdist'] = 0.0  # Crowding distance

    num_obs = len(pop[0]['objectives'])

    for o in range(num_obs):
        obj_o = [pop[i]['objectives'][o] for i in front]

        # Sort by objective value
        obj_o[:], front[:] = zip(*sorted(zip(obj_o, front), reverse=False))

        # Solution with best objective has cdist = INF
        pop[front[0]]['cdist'] = INF

        rge_o = max(obj_o) - min(obj_o)  # Range
        if rge_o < 0.0001:
            # Solutions all have same objective; cdist=0.0
            continue

        # Solutions' neighbours determine their cdist
        for i in range(1, len(front)-1):
            pop[front[i]]['cdist'] += (obj_o[i+1] - obj_o[i-1]) / rge_o


def better(p1, p2):
    # Adapted from Brownlee's clever algorithms
    # Smaller rank wins, then larger crowding distance
    if p1.get('cdist') and p1['rank'] == p2['rank']:
        if p1['cdist'] > p2['cdist']:
            return p1
        return p2

    if p1['rank'] < p2['rank']:
        return p1
    return p2


def selection(pop):
    # Adapted from Brownlee's clever algorithms
    # Pick best solutions by rank
    fronts = fast_nondominated_sort(pop)
    for front in fronts:
        calculate_crowding_distance(pop, front)

    offspring = []
    last_front = 0

    for front in fronts:
        # Compile solutions from all ranks, up to the desired population size
        if (len(offspring) + len(front)) > POP_SIZE:
            # Don't add next rank if we'll exceed desired population size
            break

        offspring += front
        last_front += 1

    remaining = POP_SIZE - len(offspring)  # Shortfall from desired population
    if remaining > 0:
        # Throw in some solutions from last front to make up shortfall
        offspring += fronts[last_front][0:remaining]

    return [pop[i] for i in offspring]


# ========== FEASIBILITY & REPAIR
def is_feasible(routeset):
    # Coverage: all nodes included?
    missing = set(range(N)).difference(i for r in routeset for i in r)
    if missing:
        return False

    # Number of routes up to par?
    if len(routeset) != R_NUM:
        return False

    # Route lengths within defined range?
    for r in routeset:
        if (len(r) < R_MIN) or (len(r) > R_MAX):
            return False

    # Simple path? (any nodes repeated in a route?)
    for i in range(N):
        for r in routeset:
            if r.count(i) > 1:
                return False

    # Routeset connected? (One node can reach all others)
    if not is_connected(route_adj(routeset)):
        return False

    return True


def repair_routeset(routeset):
    # Attempts to add missing nodes; just adds more nodes if none missing
    # Modified from Mumford 2013's repair algorithm
    chosen = set(i for r in routeset for i in r)
    missing = set(range(N)).difference(chosen)

    for r in routeset:
        i = r[-1]
        route_reversed = False

        while len(r) < R_MAX:
            # If missing node found adjacent, append it
            found = list(ADJ[i].intersection(missing))

            if found:
                i = randchoice(found)
                r.append(i)
                missing.remove(i)

                if missing:
                    continue
                else:
                    break

            # Otherwise, append some other node
            unused = list(ADJ[i].difference(r))

            if unused:
                i = randchoice(unused)
                r.append(i)
                continue

            # If all fails, try other end
            if route_reversed:
                break

            r.reverse()
            i = r[-1]
            route_reversed = True

    return is_feasible(routeset)


# ========== ROUTESET CREATION
def generate_route(routeset):
    # Algorithm of Shih and Mahmassani 1994

    rdist, rnext = floyd_warshall(route_adj(routeset))
    for u, v, d in DEMAND_PAIRS:
        # Exclude satisfied node pairs (those that need no transfers)
        satisfied = False
        for r in routeset:
            if (u in r) and (v in r):
                satisfied = True
                break

        if satisfied:
            continue

        # path = floyd_warshall_path(rnext, u, v)
        # curr_cost = rdist[u][v] + TRANSFER_PENALTY * min_transfers(routeset, path)

        # new_paths = k_shortest_paths(ADJ, u, v, 10)
        # for path, cost in new_paths:
        #     if (len(path) >= R_MIN) and (len(path) <= R_MAX) and (cost < curr_cost):
        #         return path

        # ---Deviation from Shih Mahmassani; k_paths method too slow when R_MIN is high
        # ---Instead, find shortest path between u & v, then build it up to R_MIN using
        # ---least-cost edges. This still keeps cost between u,v cheaper anyway,
        # ---which is the objective
        curr_cost = rdist[u][v] + TRANSFER_PENALTY  # Assume 1 transfer, for speed
        cost = ALL_RDIST[u][v]
        if curr_cost <= cost:
            continue

        r = floyd_warshall_path(ALL_RNEXT, u, v)
        if len(r) > R_MAX:
            # Can't fix path if too many stops
            continue

        if len(r) >= R_MIN:
            return r

        # Try to fix route that's too short
        while len(r) < R_MIN:
            # Edges that can extend r
            edges_available = []

            for u_r in [r[0], r[-1]]:
                for v_r in ADJ[u_r]:
                    if v_r not in r:
                        edges_available.append((u_r, v_r, TTIMES[u_r][v_r]))

            if edges_available:
                # Append potential edge of least cost
                edges_available.sort(key=lambda x: x[-1], reverse=False)

                u_r, v_r, t_r = edges_available[0]
                if u_r == r[0]:
                    r.reverse()
                r.append(v_r)
            else:
                # Can't fix r
                break

        if len(r) == R_MIN:
            return r
    return []


def john_routeset():
    # Heuristic routeset construction from John thesis

    # Choose random lambdas and compute weight matrix
    l1 = randchoice(np.arange(0.0, 1.01, 0.05))
    if l1 == 0.0:
        l2 = randchoice(np.arange(0.05, 1.01, 0.05))
    else:
        l2 = randchoice(np.arange(0.00, 1.01, 0.05))

    weights = [[INF]*N for i in range(N)]
    for row in range(N):
        for col in range(N):
            t = NORM_TTIMES[row][col]
            if t == INF:
                continue

            d = NORM_DEMANDS[row][col]
            weights[row][col] = l1*t + l2*(1-d)

    routeset = []
    chosen = set()

    while len(chosen) != N:
        r = []

        if len(routeset) == 0:
            # Seed first route with smallest-cost edge
            min_weight = min([cell for row in weights for cell in row])

            rowlist = list(range(N))
            randshuffle(rowlist)
            for row in rowlist:
                if min_weight in weights[row]:
                    u = row
                    v = weights[row].index(min_weight)
                    break

            r.extend([u, v])
            chosen.update([u, v])
        else:
            # Seed new route with smallest-cost edge not already in routeset
            min_u = None
            min_v = None
            min_weight = INF

            for u in chosen:
                for v in ADJ[u]:
                    if weights[u][v] < min_weight and \
                            v not in chosen:
                        min_u = u
                        min_v = v
                        min_weight = weights[u][v]

            r.extend([min_u, min_v])
            chosen.update([min_u, min_v])

        while len(r) < R_MAX:
            # Find all edges that can extend terminal nodes
            edges_available = []
            nodes_available = set()

            for u in [r[0], r[-1]]:
                for v in ADJ[u]:
                    if v not in r:
                        edges_available.append((u, v, weights[u][v]))
                        nodes_available.add(v)

            # Sort potential edges by weight
            edges_available.sort(key=lambda x: x[-1], reverse=False)
            edge_inserted = False

            # Give priority to nodes missing from routeset
            missing = nodes_available.difference(chosen)
            if missing:
                for u, v, t in edges_available:
                    if v in missing:
                        if u == r[0]:
                            r.reverse()
                        r.append(v)
                        chosen.add(v)
                        edge_inserted = True
                        break

            # No attachable missing nodes; relax priority
            if not edge_inserted:
                for u, v, t in edges_available:
                    if u == r[0]:
                        r.reverse()
                    r.append(v)
                    chosen.add(v)
                    edge_inserted = True
                    break

            # Can't attach any more nodes to this route
            if not edge_inserted:
                break

        routeset.append(r)

    # All nodes now covered; fill up routeset
    while len(routeset) < R_NUM:
        routeset.append(generate_route(routeset))

    if is_feasible(routeset):
        return routeset

    if repair_routeset(routeset):
        return routeset

    return []


def mumford_routeset():
    # Mumford 2013's routeset generation algorithm
    routeset = []
    chosen = set()  # To ensure connectivity

    for rn in range(R_NUM):
        # Select first node in new route
        if rn == 0:  # For first route in routeset
            i = randint(0, N-1)  # Start from random node
            chosen.add(i)
        else:
            i = randchoice(list(chosen))  # Connect to a previous route

        routeset.append([i])  # Initialize new route

        # Build rest of new route
        route_len = randint(R_MIN, R_MAX)  # Choose random length for this route
        route_reversed = False  # Has this route already been reversed?

        while len(routeset[rn]) < route_len:
            # Nodes adjacent to i, but not in current route
            unused = list(ADJ[i].difference(routeset[rn]))

            if unused:
                i = randchoice(unused)  # Jump to an adjacent node
                chosen.add(i)
                routeset[rn].append(i)
            else:
                if route_reversed:
                    # Can't grow route from either end: abort
                    break

                # Try to grow route from other end
                i = routeset[rn][0]
                routeset[rn].reverse()
                route_reversed = True

    if is_feasible(routeset):
        return routeset

    if repair_routeset(routeset):
        return routeset

    return []


# ========== MUTATION
def add_nodes(routeset, num_nodes):
    # Add some extra nodes
    # Operator proposed by Mumford 2013
    mutated = False
    added = 0
    routelist = list(range(R_NUM))
    randshuffle(routelist)

    for rn in routelist:
        r = routeset[rn]
        i = r[-1]
        route_reversed = False

        while len(r) < R_MAX:
            unused = list(ADJ[i].difference(r))

            if unused:
                i = randchoice(unused)
                r.append(i)
                mutated = True
                added += 1

                if added == num_nodes:
                    return mutated
            else:
                if route_reversed:
                    break

                r.reverse()
                i = r[-1]
                route_reversed = True

    return mutated


def del_nodes(routeset, num_nodes):
    # Remove some nodes
    # Operator proposed by Mumford 2013
    mutated = False
    deleted = 0
    routelist = list(range(R_NUM))
    randshuffle(routelist)

    for rn in routelist:
        r = routeset[rn]
        route_reversed = False

        while len(r) > R_MIN:
            i = r.pop()

            if is_feasible(routeset):
                mutated = True
                deleted += 1

                if deleted == num_nodes:
                    return mutated
            else:
                r.append(i)

                if route_reversed:
                    break

                r.reverse()
                route_reversed = True

    return mutated


def exchange(routeset):
    # Splits two routes at a common node & flips
    # Proposed by Mandl 1979
    mutated = False
    routelist = list(range(R_NUM))
    randshuffle(routelist)

    # Select first random route
    rn_a = routelist.pop()
    r_a = routeset[rn_a]

    for rn_b in routelist:
        # Select second random route that connects to first
        r_b = routeset[rn_b]
        joints = list(set(r_a).intersection(set(r_b)))

        if joints:
            # Split at common node and exchange segments
            i = randchoice(joints)
            split_a = r_a.index(i)
            split_b = r_b.index(i)

            r_c = r_a[0:split_a] + r_b[split_b:]
            r_d = r_b[0:split_b] + r_a[split_a:]

            ex_routeset = [r_c, r_d]

            for rn in routelist:
                if rn == rn_b:  # Can't be rn_a: already popped
                    continue
                ex_routeset.append(copy(routeset[rn]))

            if is_feasible(ex_routeset):
                routeset[:] = copy(ex_routeset)
                mutated = True
                return mutated

    return mutated


def merge(routeset):
    # Proposed by John et al 2014
    # Merge two routes into one, then insert a new one
    mutated = False
    routelist = list(range(R_NUM))
    randshuffle(routelist)

    for a in range(R_NUM-1):
        rn_a = routelist[a]
        r_a = routeset[rn_a]

        for b in range(a+1, R_NUM):
            rn_b = routelist[b]
            r_b = routeset[rn_b]

            # If routes a and b have common terminal nodes, join them
            if R_MIN <= (len(r_a) + len(r_b) - 1) <= R_MAX and \
                    len({r_a[0], r_a[-1]}.intersection({r_b[0], r_b[-1]})) == 1 and \
                    len(set(r_a).intersection(r_b)) == 1:
                if r_a[0] == r_b[0]:
                    r_c = copy(r_a)
                    r_c.reverse()
                    r_c.pop()
                    r_c.extend(r_b)
                elif r_a[-1] == r_b[0]:
                    r_c = copy(r_a)
                    r_c.pop()
                    r_c.extend(r_b)
                elif r_a[0] == r_b[-1]:
                    r_c = copy(r_b)
                    r_c.pop()
                    r_c.extend(r_a)
                else:
                    r_c = copy(r_a)
                    r_c.pop()
                    r_c.extend(reversed(r_b))

                mg_routeset = [r_c]
                for rn in range(R_NUM):
                    if (rn != rn_a) and (rn != rn_b):
                        mg_routeset.append(copy(routeset[rn]))

                # Insert a new route to make up the route number
                mg_routeset.append(generate_route(mg_routeset))

                if is_feasible(mg_routeset):
                    routeset[:] = mg_routeset
                    mutated = True

                return mutated
    return mutated


def replace(routeset):
    # Proposed by John et al 2014
    # Replace route that serves least demand
    mutated = False
    demands = []

    # Find least-demand-route
    for r in routeset:
        demand = 0
        for i in range(len(r)-1):
            u = r[i]
            v = r[i+1]
            demand += DEMANDS[u][v]
        demands.append(demand)

    min_rn = demands.index(min(demands))  # Might be more than one?

    # Replace least-demand-route with a new one
    rep_routeset = []
    for rn in range(R_NUM):
        if rn == min_rn:
            continue
        rep_routeset.append(copy(routeset[rn]))

    rep_routeset.append(generate_route(rep_routeset))

    if is_feasible(rep_routeset):
        routeset[:] = rep_routeset
        mutated = True
        return mutated

    if repair_routeset(rep_routeset):
        routeset[:] = rep_routeset
        mutated = True
        return mutated

    return mutated


def remove_overlapping(routeset):
    # Proposed by John et al 2014
    # Replace route that is a subset of another
    mutated = False

    # Find an overlapping route
    rem_rn = None
    for rn in range(R_NUM-1):
        # TODO: OVERLAPPING
        r_a = routeset[rn]
        r_b = routeset[rn+1]

        if len(r_a) <= len(r_b):
            if set(r_a).issubset(r_b):
                rem_rn = rn
                break
        else:
            if set(r_b).issubset(r_a):
                rem_rn = rn + 1
                break

    if rem_rn is None:
        return mutated

    # Replace  overlapping route with a new one
    rem_routeset = []
    for rn in range(R_NUM):
        if rn == rem_rn:
            continue
        rem_routeset.append(copy(routeset[rn]))

    rem_routeset.append(generate_route(rem_routeset))

    if is_feasible(rem_routeset):
        routeset[:] = rem_routeset
        mutated = True

    return mutated


def mutate(routeset):
    if random() > P_MUTATE:
        return

    # Equal probability mutation operators
    op = randint(0, 5)
    if op == 0:
        num_nodes = randint(1, (R_NUM * R_MAX) // 2)
        del_nodes(routeset, num_nodes)

    elif op == 1:
        num_nodes = randint(1, (R_NUM * R_MAX) // 2)
        add_nodes(routeset, num_nodes)

    elif op == 2:
        exchange(routeset)

    elif op == 3:
        merge(routeset)

    elif op == 4:
        replace(routeset)

    else:
        remove_overlapping(routeset)

    return


# ========== REPRODUCTION
def crossover(parent1, parent2):
    # Pick half of routes from each parent
    # Algorithm described in Mumford 2013

    if random() < P_CROSS:
        childset = []
        parentsets = [parent1, parent2]

        routelists = [list(range(R_NUM)), list(range(R_NUM))]
        randshuffle(routelists[0])
        randshuffle(routelists[1])

        # Start from random route
        rn = routelists[0].pop()
        childset.append(copy(parentsets[0][rn]))
        chosen = set(parentsets[0][rn])

        parent = 1
        for i in range(R_NUM*4):
            parentset = parentsets[parent]
            routelist = routelists[parent]
            best_rn = None
            best_ratio = -1

            for rn in routelist:
                r = parentset[rn]
                inchosen = len(chosen.intersection(r))

                # Next route must maintain connection
                if inchosen == 0:
                    continue

                # Route with highest % of new nodes wins
                new_ratio = 1 - (inchosen / len(r))

                if new_ratio > best_ratio:
                    best_ratio = new_ratio
                    best_rn = rn

            if best_rn is not None:
                childset.append(copy(parentset[best_rn]))

                if len(childset) == R_NUM:
                    break

                chosen.update(parentset[best_rn])
                routelist.remove(best_rn)

            parent = 1 - parent  # Next parent

        if is_feasible(childset):
            return childset

        if repair_routeset(childset):
            return childset

    return parent1


def reproduce(selected):
    # Adapted from Brownlee's clever algorithms
    # Mate in pairs; 'selected' population is already
    # randomized from previous binary tournament
    num_sel = len(selected)
    children = []

    for i in range(num_sel):
        # Each pair mates twice to produce 2 children
        if i % 2 == 0:
            j = i + 1
        else:
            j = i - 1

        if i == (num_sel - 1):
            j = 0

        p1 = selected[i]
        p2 = selected[j]

        child = dict()
        child['routeset'] = crossover(p1['routeset'], p2['routeset'])
        mutate(child['routeset'])
        children.append(child)

        if i >= POP_SIZE:  # Probably unnecessary
            break

    return children


# ========== PLOTTING & OUTPUT
def print_routeset(routeset):
    for r in routeset:
        print(r)
    print('---------------------')


def plot_routeset(routeset, title, save=False):
    r_num = len(routeset)
    x, y = zip(*COORDS)
    colors = ['blue', 'orange', 'yellow', 'green', 'red',
              'magenta', 'brown', 'gold', 'lightgreen', 'lightblue',
              'black', 'chocolate', 'coral', 'teal', 'olive']
    linestyles = ['solid', 'dashed']
    linewidths = [i/2 for i in range(10, 3, -1)]

    if save:
        fig = plt.figure(figsize=(12, 12))
    else:
        fig = plt.figure()

    plt.scatter(x, y, s=20, c='k')
    plt.title(title)

    # Plot bus stops
    for i in range(N):
        plt.annotate(i, xy=(x[i], y[i]), xytext=(-2, 2),
                     path_effects=[patheffects.withStroke(
                         linewidth=1, foreground="w")],
                     textcoords='offset points',
                     ha='right', va='bottom',
                     fontsize=8)

    # Plot route connections
    for rn in range(r_num):
        for i in range(len(routeset[rn])-1):
            u, v = routeset[rn][i], routeset[rn][i+1]
            plt.plot((x[u], x[v]),
                     (y[u], y[v]),
                     ls=linestyles[rn % 2],
                     lw=linewidths[rn % 7],
                     c=colors[rn % 15],
                     alpha=0.9)

    if save:
        plt.savefig(title + ".png")
        plt.close(fig)
    else:
        plt.draw()
        plt.pause(0.001)


def plot_objectives(pop):
    x, y = zip(*[p['objectives'] for p in pop])

    plt.scatter(x, y, s=5)
    plt.draw()
    plt.pause(0.001)


def plot_results(pop):
    top = [p for p in pop if p['rank'] == 0]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('Passenger Cost (minutes)')
    ax.set_ylabel('Operator Cost (minutes)')

    plot_objectives(pop)
    plot_objectives(top)

    top.sort(key=lambda k: k['objectives'][0], reverse=False)
    objs = "p:{:.2f}, o:{:.2f}".format(top[0]['objectives'][0], top[0]['objectives'][1])

    plot_routeset(top[0]['routeset'], title="Best for Passengers: {}".format(objs))

    top.sort(key=lambda k: k['objectives'][1], reverse=False)
    objs = "p:{:.2f}, o:{:.2f}".format(top[0]['objectives'][0], top[0]['objectives'][1])

    plot_routeset(top[0]['routeset'], title="Best for Operator: {}".format(objs))

    plt.show()


def save_results(pop):
    path = os.path.dirname(os.path.realpath(__file__))
    routefile_prefix = path + "/results/" + DATASET + "_Set"
    qualityfile = path + "/results/" + DATASET + "_Quality"

    top = [p for p in pop if p['rank'] == 0]  # Top rank
    qualitymetrics = []

    # Save routesets (in separate files)
    for i in range(len(top)):
        routefile = routefile_prefix + str(i + 1) + ".txt"

        with open(routefile, 'w') as outputfile:
            for r in top[i]['routeset']:
                outputfile.write(" ".join(map(str, r)) + "\r\n")

        qualitymetrics.append(calculate_quality(top[i]['routeset']))

    # Save quality metrics (in single file)
    with open(qualityfile, 'w') as outputfile:
        outputfile.write("Set ATT RL d0 d1 d2 dun shortest longest\r\n")

        for i in range(len(qualitymetrics)):
            row = qualitymetrics[i]
            outputfile.write(str(i+1) + " " + " ".join(map(str, row)) + "\r\n")


# ========== SEARCH PROCEDURE
def search():
    # General NSGAII search procedure
    # Adapted from Brownlee's clever algorithms
    print("CREATING INITIAL POPULATION...")
    pop = []

    while True:
        routeset = john_routeset()
        # routeset = mumford_routeset()

        if routeset:
            pop.append({'routeset': routeset})
            print(" > added routeset {0}...".format(len(pop)))

            if len(pop) == POP_SIZE:
                break

    print("PREPARING GENERATION ZERO...")
    calculate_objectives(pop)
    fast_nondominated_sort(pop)  # Assign ranks

    parents = []  # Binary tournament for mating
    for i in range(POP_SIZE):
        parents.append(better(pop[randint(0, POP_SIZE-1)],
                              pop[randint(0, POP_SIZE-1)]))

    children = reproduce(parents)
    calculate_objectives(children)

    print("SEARCHING FOR BEST ROUTES...")
    best_cp = INF  # Best passenger cost
    best_co = INF  # Best operator cost
    best_sp = {'objectives': [INF, INF]}  # Best passenger solution
    best_so = {'objectives': [INF, INF]}  # Best operator solution

    for gen in range(MAX_GENS):
        chrm_pool = pop + children  # Two-generation chromosome pool
        mate_pool = selection(chrm_pool)  # Get quality mates

        parents = []
        for i in range(POP_SIZE):
            parents.append(better(mate_pool[randint(0, POP_SIZE-1)],
                                  mate_pool[randint(0, POP_SIZE-1)]))

        pop = deepcopy(children)
        children = reproduce(parents)
        calculate_objectives(children)

        # Memorize best extreme solutions
        cp = [p['objectives'][0] for p in mate_pool]
        co = [p['objectives'][1] for p in mate_pool]
        min_cp = min(cp)
        min_co = min(co)

        if min_cp <= best_cp:
            i = cp.index(min_cp)
            sp = mate_pool[i]
            if dominates(sp, best_sp) == 1:
                best_sp = copy(sp)
                best_cp = min_cp

        if min_co <= best_co:
            i = co.index(min_co)
            so = mate_pool[i]
            if dominates(so, best_so) == 1:
                best_so = copy(so)
                best_co = min_co

        # Pretty print progress
        print(" > gen={:3d}, best passenger={:3.2f}, best operator={:3.2f}".
              format(gen+1, min_cp, min_co))

    final_cut = selection(pop + children + [best_sp, best_so])
    return final_cut


# ========== TEST BENCH
def run_test(scenario=0, plot=False, save=False, profile=False):
    # Run a test scenario
    if scenario == 0:
        dataset = "Mandl"  # N = 15
        r_num = 6
        r_min = 2
        r_max = 8
        p_cross = 0.9
        pop_size = 100
        max_gens = 50

    elif scenario == 1:
        dataset = "Mumford0"  # N = 30
        r_num = 12
        r_min = 2
        r_max = 15
        p_cross = 0.9
        pop_size = 200
        max_gens = 200

    elif scenario == 2:
        dataset = "Mumford1"  # N = 70
        r_num = 15
        r_min = 10
        r_max = 30
        p_cross = 0.9
        pop_size = 200
        max_gens = 200

    elif scenario == 3:
        dataset = "Mumford2"  # N = 110
        r_num = 56
        r_min = 10
        r_max = 22
        p_cross = 0.9
        pop_size = 200
        max_gens = 200

    elif scenario == 4:
        dataset = "Mumford3"  # N = 127
        r_num = 60
        r_min = 12
        r_max = 25
        p_cross = 0.9
        pop_size = 200
        max_gens = 200

    else:
        dataset = "Nottingham600"  # N = 274
        r_num = 40
        r_min = 2
        r_max = 30
        p_cross = 0.9
        pop_size = 200
        max_gens = 200

    print('LOADING {} DATASET...'.format(dataset))

    load_data(dataset)
    set_params(r_num, r_min, r_max, p_cross, pop_size, max_gens)

    print(' > LOADED {}, N = {}'.format(dataset, N))

    if profile:
        cProfile.run('search()')

    else:
        solutions = search()

        if save:
            save_results(solutions)

        if plot:
            plot_results(solutions)

run_test(scenario=0,  plot=False, save=True, profile=False)

