#!/usr/bin/env python

from __future__ import absolute_import, print_function, division
from matplotlib import pyplot, patheffects
import os


class RoutesetPlotter:

    def __init__(self):
        self.coordspath = ""
        self.x = []
        self.y = []
        self.nodecount = 0

        # Default plotting styles
        self.style = dict()
        self.style['plotwidth'] = 12
        self.style['plotheight'] = 12

        self.style['fontsize'] = 8
        self.style['nodesize'] = 20
        self.style['nodecolor'] = 'black'

        self.style['linealpha'] = 0.9
        self.style['linestyles'] = ['solid', 'dashed']
        self.style['linewidths'] = [i/2 for i in range(10, 3, -1)]
        self.style['linecolors'] = ['blue', 'orange', 'yellow', 'green', 'red',
                                    'magenta', 'brown', 'gold', 'lightgreen',
                                    'lightblue', 'black', 'chocolate', 'coral',
                                    'teal', 'olive']

    def set_style(self, style_name, style_value):
        self.style[style_name] = style_value

    def load_coords(self, coordsfile):
        basepath = os.path.dirname(os.path.realpath(__file__))
        self.coordspath = os.path.join(basepath, coordsfile)

        with open(self.coordspath) as inputfile:
            self.nodecount = int(inputfile.readline())
            assert self.nodecount > 0, \
                "Error in Node Count. Exiting..."

            for line in inputfile:
                coords = line.strip().split()
                if coords:
                    self.x.append(float(coords[0]))
                    self.y.append(float(coords[1]))

    def plot_routeset(self, routeset_file, combined=True, outfolder=None):
        basepath = os.path.dirname(os.path.realpath(__file__))
        filepath = os.path.join(basepath, routeset_file)
        filename = os.path.splitext(os.path.basename(filepath))[0]

        if outfolder is None:
            outfolder = os.path.dirname(filepath)

        else:
            outfolder = os.path.join(basepath, outfolder)
            if not os.path.isdir(outfolder):
                os.mkdir(outfolder)

        # Load routeset
        routeset = []
        with open(filepath) as inputfile:
            for line in inputfile:
                r = line.strip().split()
                if r:
                    routeset.append(list(map(int, r)))

        r_num = len(routeset)

        # Convert 1-indexed routeset to 0-indexed
        routeset_nodes = set(i for r in routeset for i in r)
        if min(routeset_nodes) == 1:
            for rn in range(r_num):
                routeset[rn] = list(map(lambda x: x-1, routeset[rn]))

            routeset_nodes = set(i for r in routeset for i in r)

        # Check for unknown nodes
        assert routeset_nodes.issubset(set(range(self.nodecount))), \
            "Error in Routeset: unknown nodes found. Exiting..."

        # Plot routeset
        if combined:
            # Combine all routes in a single plot
            print("> PLOTTING COMBINED ROUTESET...")
            fig = pyplot.figure(figsize=(self.style['plotwidth'], self.style['plotheight']))
            self.plot_nodes()

            for rn in range(r_num):
                print("    * Plotting route {} of {}...".format(rn+1, r_num))
                self.plot_route(routeset[rn], rn+1)

            pyplot.savefig("{}.png".format(os.path.join(outfolder, filename)))
            pyplot.close(fig)

        else:
            # Create a separate plot for each route
            print("> PLOTTING SEPARATE ROUTES...")
            for rn in range(r_num):
                print("    * Plotting route {} of {}...".format(rn+1, r_num))
                fig = pyplot.figure(figsize=(self.style['plotwidth'], self.style['plotheight']))
                self.plot_nodes()
                self.plot_route(routeset[rn])

                pyplot.savefig("{}_{}.png".format(os.path.join(outfolder, filename), rn+1))
                pyplot.close(fig)

    def plot_nodes(self):
        pyplot.scatter(self.x, self.y,
                       s=self.style['nodesize'],
                       c=self.style['nodecolor'])

        for i in range(self.nodecount):
            pyplot.annotate(i, xy=(self.x[i], self.y[i]), xytext=(-2, 2),
                            path_effects=[patheffects.withStroke(linewidth=1, foreground="w")],
                            textcoords='offset points',
                            ha='right', va='bottom',
                            fontsize=self.style['fontsize'])

    def plot_route(self, route, style_index=1):
        for i in range(len(route)-1):
            u, v = route[i], route[i+1]
            pyplot.plot((self.x[u], self.x[v]),
                        (self.y[u], self.y[v]),
                        ls=self.style['linestyles'][style_index % len(self.style['linestyles'])],
                        lw=self.style['linewidths'][style_index % len(self.style['linewidths'])],
                        c=self.style['linecolors'][style_index % len(self.style['linecolors'])],
                        alpha=self.style['linealpha'])


# USAGE EXAMPLE
plotter = RoutesetPlotter()
plotter.set_style("plotwidth", 15)
plotter.set_style("plotheight", 15)

plotter.load_coords("instances/Nottingham600_20171208/Nottingham600_Coords.txt")

plotter.plot_routeset("results/Nottingham600_20171205/operator.txt",
                      combined=True, outfolder="plots")
plotter.plot_routeset("results/Nottingham600_20171205/operator.txt",
                      combined=False, outfolder="plots")

plotter.plot_routeset("results/Nottingham600_20171205/passenger.txt",
                      combined=True, outfolder="plots")
plotter.plot_routeset("results/Nottingham600_20171205/passenger.txt",
                      combined=False, outfolder="plots")
